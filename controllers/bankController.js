const fs = require('fs');
const path = require('path');

const banksPath = path.join(
	__dirname,
	'../controllers/..',
	'data',
	'banks.json'
);

exports.getBanks = function (req, res) {
	fs.readFile(banksPath, 'utf8', (err, data) => {
		if (err) {
			res.json([]);
		} else {
			const banks = JSON.parse(data);
			res.json(banks);
		}
	});
};

exports.createBank = function (req, res) {
	const bank = req.body;
	fs.readFile(banksPath, 'utf8', (err, data) => {
		let banks = err ? bank : [bank, ...JSON.parse(data)];

		fs.writeFile(banksPath, JSON.stringify(banks), err => {
			if (err) {
				res.status(500).send(err);
			} else {
				res.json(bank);
			}
		});
	});
};

exports.changeBank = function (req, res) {
	const bank = req.body;

	fs.readFile(banksPath, 'utf8', (err, data) => {
		if (err) {
			res.status(500).send(err);
		} else {
			const banks = JSON.parse(data);

			const bankNumber = banks.findIndex(b => b.id === req.params.id);
			if (bankNumber === -1) {
				res.status(500).send('Incorrect bank');
			} else {
				banks[bankNumber] = bank;
				fs.writeFile(banksPath, JSON.stringify(banks), err => {
					if (err) res.status(500).send(err);
				});

				res.json(bank);
			}
		}
	});
};

exports.dropBank = function (req, res) {
	const bankName = req.params.name;

	fs.readFile(banksPath, 'utf8', (err, data) => {
		if (err) {
			res.status(500).send(err);
		} else {
			const banks = JSON.parse(data);
			const bankNumber = banks.findIndex(b => b.id === req.params.id);

			if (bankNumber === -1) {
				res.status(500).send('Incorrect bank');
			} else {
				const droppedBank = banks.splice(bankNumber, 1);

				fs.writeFile(banksPath, JSON.stringify(banks), err => {
					if (err) {
						res.status(500).send(err);
					} else {
						res.json(droppedBank[0]);
					}
				});
			}
		}
	});
};
