class Bank {
	static counter = 0;

	constructor(name, rate, maxLoan, minDownPaymentRate, maxLoanTerm) {
		this.id = Bank.counter;
		this.name = name;
		this.rate = rate;
		this.maxLoan = maxLoan;
		this.minDownPaymentRate = minDownPaymentRate;
		this.maxLoanTerm = maxLoanTerm;

		Bank.counter++;
	}
}

export default Bank;
