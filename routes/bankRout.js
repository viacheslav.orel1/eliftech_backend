const { Router } = require('express');
const {
	getBanks,
	createBank,
	changeBank,
	dropBank,
} = require('../controllers/bankController');

const router = Router();

router.get('/api/bank', getBanks);
router.post('/api/bank', createBank);
router.put('/api/bank/:id', changeBank);
router.delete('/api/bank/:id', dropBank);

module.exports = router;
