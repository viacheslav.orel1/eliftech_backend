const express = require('express');
const path = require('path');
const bankRouter = require('./routes/bankRout');

// configure express
const app = express();

app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

// configure routes
app.use('/', bankRouter);

app.listen(3001, () => console.log('Server has been started'));
